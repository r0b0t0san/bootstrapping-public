#!/bin/bash

# ===========================
# TODO
# - trouble command -v if logic for program install checks to prevent reinstall attempt 
# ===========================

# ===========================
# README
# - bootstrap script for xubuntu 20.04.3
# - installs apt packages, sets profiles
# - installs nomachine, librewolf, vscodium, unifi
# ===========================

# ===========================
# Sources
# https://gist.github.com/59023g/cb632616a7c39fcd0f9e11190617efbe
# https://knowledgebase.nomachine.com/AR01L00775
# https://kifarunix.com/install-nomachine-on-ubuntu/
# https://www.how2shout.com/linux/how-to-install-librewolf-browser-on-ubuntu-20-04-lts/
# https://librewolf.net/installation/debian/
# https://www.how2shout.com/linux/install-vscodium-on-ubuntu-22-04-20-04-linux/
# https://vscodium.com/#install
# ===========================

# ===========================
# Functions
# ===========================
progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

update_apt_repo(){
  LAST_UPDATED=$( stat --format='%X' /var/cache/apt/pkgcache.bin )
  UNIX_TIME=$( date +%s )
  TIME_DIFF=$(( UNIX_TIME - LAST_UPDATED ))
  if [[ "${TIME_DIFF}" -gt 43200 ]]
  then
    sudo apt update
  else
    progress_bar '[!] --- APT REPO IS WITH WITHIN 12 HOURS ---'
  fi
}

install_additional_packages(){
  packages=(
    'caffeine'
    'cmatrix'
    'cowsay-off'
    'fail2ban'
    'figlet'
    'fortunes-off'
    'git'
    'haveged'
    'htop'
    'openssh-server' 
    'tmux'
    'tree'
    'vim'
  )
  for i in "${packages[@]}"
  do
    if ! [ -x "$(command -v $i)" ]
    then
      echo "[+] --- INSTALLING: $i ---"
      sudo apt --yes install $i
    else
      echo "[!] --- ALREADY INSTALLED $i---"
    fi
  done
  progress_bar '[+] --- ADDITIONAL PACKAGES FINISHED ---'
  sudo apt autoclean -y
  sudo apt autoremove -y
}

set_profiles(){
  #touch $HOME/.bash_aliases
  cp ./bash_alias_template.txt $HOME/.bash_aliases

}

install_nomachine(){
  wget https://www.nomachine.com/free/linux/64/tar -O nomachine.tar.gz
  sudo tar -xzvf nomachine.tar.gz -C /usr
  sudo /usr/NX/nxserver --install
  sudo mv ./nomachine.tar.gz /opt
  progress_bar '[+] --- NOMACHINE INSTALLED ---'
}

install_librewolf(){
  echo "deb [arch=amd64] http://deb.librewolf.net focal main" | sudo tee /etc/apt/sources.list.d/librewolf.list
  sudo wget https://deb.librewolf.net/keyring.gpg -O /etc/apt/trusted.gpg.d/librewolf.gpg
  sudo apt update
  sudo apt install librewolf -y
  progress_bar '[+] --- LIBREWOLF INSTALLED ---'
}

install_codium(){
  wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
  echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
  sudo apt update
  sudo apt install codium -y
  progress_bar '[+] --- CODIUM INSTALLED ---'
}

install_unifi(){
  sudo apt install --yes apt-transport-https
  echo 'deb https://www.ui.com/downloads/unifi/debian stable ubiquiti' \
    | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
  sudo wget -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg
  progress_bar '[+] --- ADDED UNIFI APT SOURCE LIST AND GPG KEY ---'
  sudo apt update
  sudo apt install --yes openjdk-8-jre-headless
  sudo apt install --yes unifi
  progress_bar '[+] --- JRE AND UNIFI INSTALLED ---'
  # change ownership from root to unifi
  sudo chown -R unifi:unifi /usr/lib/unifi
  sudo systemctl status --no-pager --full mongodb.service unifi.service
  sleep 3
  progress_bar '[+] --- RESTARTING UNIFI ---'
  sudo systemctl restart unifi.service
  sudo systemctl status --no-pager --full unifi.service
}

main_script(){
  progress_bar '[+] --- SETTING PREFERRED SETTINGS ---'
  set_profiles

  progress_bar '[+] --- UPGRADING AND INSTALLING PACKAGES... ---'
  update_apt_repo
  sudo apt upgrade -y
  progress_bar '[+] --- SYSTEM UPGRADES FINISHED ---'
  install_additional_packages

  # check if reboot required before attempting other packages
  if [ -f /var/run/reboot-required ]
  then
    progress_bar '[!] --- FINISHED PACKAGE INSTALLS, REBOOT REQUIRED BEFORE CONTINUING ---'
    exit 0
  else
    progress_bar '[+] --- FINISHED PACKAGE INSTALLS, NO REBOOT REQUIRED ---'
  fi

  # nomachine install, server will start automatically after install
  progress_bar '[+] --- NOMACHINE ---'
  # check if nomachine is installed; looks for nxserver command or nxserver file
  # TROUBLESHOOT IF LOGIC
  if [[ $(command -v nxserver &> /dev/null) || -f /usr/NX/bin/nxserver ]]
  then
    progress_bar '[!] --- NOMACHINE ALREADY INSTALLED ---'
  else
    install_nomachine  
  fi

  # librewolf install
  progress_bar '[+] --- LIBREWOLF ---'
  # TROUBLESHOOT IF LOGIC
  if [[ $(command -v librewolf &> /dev/null) ]]
  then
    progress_bar '[!] --- LIBREWOLF ALREADY INSTALLED ---'
  else
    install_librewolf
  fi

  # codium install
  progress_bar '[+] --- CODIUM ---'
  # TROUBLESHOOT IF LOGIC
  if [[ $(command -v codium &> /dev/null) ]]
  then
    progress_bar '[!] --- CODIUM ALREADY INSTALLED ---'
  else
    install_codium
  fi

  # unifi install
  progress_bar '[+] --- UNIFI ---'
  # check if unifi service is present; need to inverse logic based on the check used
  # TROUBLESHOOT IF LOGIC
  if [[ $(sudo systemctl list-units --no-pager --full | grep -Fq "unifi.service") ]]
  then 
    progress_bar '[!] --- UNIFI SERVICE ALREADY PRESENT ---'
  else
    install_unifi
  fi
  progress_bar '[+] LOG INTO THE UNIFI CONTROLLER AT: https://unifi_hostname:8443'
}


# ===========================
# Main Script
# ===========================
progress_bar '[+] --- STARTING SCRIPT... ---'
main_script
# finished, check if reboot required
sudo apt autoclean -y
sudo apt autoremove -y
if [[ -f /var/run/reboot-required ]]
then
  progress_bar '[+] --- SCRIPT COMPLETE, REBOOT HOST ---'
else
  progress_bar '[+] --- SCRIPT COMPLETE, NO REBOOT REQUIRED ---'
fi
exit 0
