#!/bin/bash

# docker install script for xubuntu 20.04
# https://docs.docker.com/engine/install/ubuntu/

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

progress_bar "[+] --- starting script ---" 

progress_bar "[+] --- removing old packages ---" 
sudo apt purge docker docker-engine docker.io containerd runc
progress_bar "[+] --- installing prerequisite packages ---" 
sudo apt-get install --yes ca-certificates curl gnupg lsb-release

# add docker gpg key and apt source list
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
  | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
  https://download.docker.com/linux/ubuntu focal stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

progress_bar "[+] --- updating apt repositories ---"
sudo apt update
progress_bar "[+] --- installing docker packages ---"
sudo apt install --yes docker-ce docker-ce-cli containerd.io docker-compose-plugin
progress_bar "[+] --- testing docker install ---"
sudo docker --version
sudo docker run hello-world

progress_bar "[+] --- script complete ---"
exit 0
