#!/bin/bash

# TODO
# troubleshoot how to share port 53 with unifi

# boostraps pihole for docker
# https://adamtheautomator.com/pihole-docker/
# https://hub.docker.com/r/pihole/pihole

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

progress_bar "[+] --- starting script ---"

progress_bar "[+] --- making pihole docker volumes ---"
sudo docker volume create pihole_app
sudo docker volume create dns_config

progress_bar "[+] --- starting pihole container with configs ---"
sudo docker run \
    --name=pihole \
    -e TZ=America/New_York \
    -e WEBPASSWORD=piholepassword \
    -e SERVERIP=10.1.1.5 \
    -v pihole_app:/etc/pihole \
    -v dns_config:/etc/dnsmasq.d \
    -p 8081:80 \
    -p 53:53/tcp \
    -p 53:53/udp \
    --restart=unless-stopped \
    pihole/pihole:2022.05

progress_bar "[+] --- log into web interface at 'http://<HOST-IP>:8081' ---"

progress_bar "[+] --- script complete ---"
exit 0