#!/bin/bash

# works on ubuntu 20.04
# installs openjdk-8-jre-headless and unifi network controller

# sources
# https://gist.github.com/davecoutts/5ccb403c3d90fcf9c8c4b1ea7616948d
# https://gist.github.com/davecoutts/5ccb403c3d90fcf9c8c4b1ea7616948d?permalink_comment_id=3699573#gistcomment-3699573
# https://davidshomelab.com/unifi-controller-setup-on-ubuntu-20-04lts/

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

progress_bar "[+] --- starting script ---"
sudo apt update
sudo apt install --yes apt-transport-https

echo 'deb https://www.ui.com/downloads/unifi/debian stable ubiquiti' | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
sudo wget -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg
progress_bar "[+] --- added unifi apt list and gpg key ---"

sudo apt update
sudo apt install --yes openjdk-8-jre-headless
sudo apt install --yes unifi
progress_bar "[+] --- jre and unifi installed ---"
sudo apt clean
# change ownership from root to unifi
sudo chown -R unifi:unifi /usr/lib/unifi
sudo systemctl status --no-pager --full mongodb.service unifi.service
sleep 3
sudo systemctl restart unifi.service
progress_bar "[-] --- restarting unifi ---"
sudo systemctl status --no-pager --full unifi.service
progress_bar "[+] log into unifi controller at https://unifi_controller_hostname:8443"


progress_bar "[+] --- script complete ---"

