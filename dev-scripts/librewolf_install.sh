#!/bin/bash

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

progress_bar "[+] --- installing librewolf... ---"
if ! command -v librewolf &> /dev/null
then
  echo "deb [arch=amd64] http://deb.librewolf.net $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/librewolf.list
  sudo wget https://deb.librewolf.net/keyring.gpg -O /etc/apt/trusted.gpg.d/librewolf.gpg
  sudo apt update
  sudo apt install librewolf -y
  progress_bar "[+] --- librewolf installed ---"
else
  progress_bar "[!] --- librewolf already installed ---"
fi
