#!/bin/bash

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

progress_bar "[+] --- starting script ---"

progress_bar "[+] --- upgrading and installing packages... ---"
LAST_UPDATED=$( stat --format="%X" /var/cache/apt/pkgcache.bin )
UNIX_TIME=$( date +%s )
TIME_DIFF=$(( UNIX_TIME - LAST_UPDATED ))
echo $TIME_DIFF
if [[ "${TIME_DIFF}" -gt 43200 ]]
then
  sudo apt update
else
  progress_bar "[!] --- apt repo is within 12 hours ---"
fi

progress_bar "[+] --- system upgrades finished ---"
packages=(
  "openssh-server" 
  "git"
  "vim"
  "tmux"
  "htop"
  "tree"
  "figlet"
  "fortunes-off"
  "cowsay-off"
  "haveged"
  "fail2ban"
)
for i in "${packages[@]}"
do
  if ! [ -x "$(command -v $i)" ]
  then
    echo "--- installing $i... ---"
    sudo apt --yes install $i
    echo "--- $i installed ---"
  else
    echo "--- $i already installed ---"
  fi
done
progress_bar "[+] --- additional packages finished ---"
sudo apt autoclean -y

