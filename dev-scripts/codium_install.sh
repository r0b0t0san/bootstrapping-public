#!/bin/bash

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

# codium install
progress_bar "[+] --- installing codium... ---"
if ! command -v codium &> /dev/null
then
  wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
  echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
  sudo apt update
  sudo apt install codium -y
  progress_bar "[+] --- codium installed ---"
else
  progress_bar "[!] --- codium already installed ---"
fi

