#!/bin/bash

progress_bar() {
  echo '==============================='
  echo $1
  echo '==============================='
}

progress_bar "[+] --- installing unifi... ---"
#if [[ $(sudo systemctl list-units --no-pager --full | grep -Fq "unifi.service") ]]
if ! [[ $(command -v librewolf &> /dev/null) ]]
then
  progress_bar "foo"
else
  # sudo systemctl status --no-pager --full mongodb.service unifi.service  
  progress_bar "bar"
fi

